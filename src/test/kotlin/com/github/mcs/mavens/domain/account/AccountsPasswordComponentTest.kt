package com.github.mcs.mavens.domain.account

import com.github.mcs.mavens.AbstractWireMockTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class AccountsPasswordComponentTest : AbstractWireMockTest() {

    private lateinit var testee: AccountsPasswordComponent

    @BeforeEach
    fun setup() {
        testee = AccountsPasswordComponent(mavensApiClient)
    }

    @Test
    fun passwordCorrect() {
        assertTrue(testee.check("good_user", "good_pass"))
    }

    @Test
    fun passwordWrong() {
        assertFalse(testee.check("good_user", "bad_pass"))
    }

    @Test
    fun usernameWrong() {
        assertFalse(testee.check("unknown_user", "good_pass"))
    }

}