package com.github.mcs.mavens.domain.account

import com.github.mcs.mavens.AbstractWireMockTest
import com.github.mcs.mavens.domain.common.Result
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class AccountsGetComponentTest : AbstractWireMockTest() {

    private lateinit var testee: AccountsGetComponent

    @BeforeEach
    fun setup() {
        testee = AccountsGetComponent(mavensApiClient)
    }

    @Test
    fun goodUser() {
        val user = testee.get("good_user")!!

        user.run {
            assertEquals(Result.Ok, result)
            assertEquals("good_user", player)
            assertEquals("adminwinsen, winsen", permissions)
            assertEquals("Marcus K.", realName)
            assertEquals("Male", gender)
            assertEquals("WL", location)
            assertEquals(1000, balance)
            assertEquals(1234, balance2)
        }
    }

    @Test
    fun noPermsUser() {
        val user = testee.get("no_perms")!!

        user.run {
            assertEquals(Result.Ok, result)
            assertEquals("no_perms", player)
            assertEquals("", permissions)
        }
    }

}