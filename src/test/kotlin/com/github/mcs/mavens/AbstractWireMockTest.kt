package com.github.mcs.mavens

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach

const val enableWireMockStubs = true    // set to false if requests should not use WireMock server
const val recordWireMockStubs = false   // set to true if WireMock proxy mode should record HTTP calls as stub files

open class AbstractWireMockTest {

    protected lateinit var mavensApiClient: MavensApiClient
    private lateinit var apiUrl: String

    companion object {
        private val wireMockServer = WireMockServer(0)

        @BeforeAll
        @JvmStatic
        private fun setupWiremockServer() {
            wireMockServer.start()
        }

        @AfterAll
        @JvmStatic
        private fun teardownWiremockServer() {
            wireMockServer.stop()
        }
    }


    @BeforeEach
    private fun setupMavensApiClient() {
        apiUrl = System.getProperty("app.api.url") ?: "http://localhost:8087/api"
        val apiPassword = System.getProperty("app.api.password") ?: "mavens-api-password"
        val requestApiUrl = if (enableWireMockStubs)
            apiUrl.replace(
                Regex("\\d+"),
                wireMockServer.port().toString()
            )
        else
            apiUrl
        mavensApiClient = MavensApiClient(requestApiUrl, apiPassword)

        if (recordWireMockStubs) {
            wireMockServer.startRecording(
                WireMock.recordSpec()
                    .forTarget(apiUrl.replace("/api", ""))
                    .makeStubsPersistent(true)
                    .extractTextBodiesOver(0)
            )
        }
    }

    @AfterEach
    private fun stopRecord() {
        if (recordWireMockStubs) {
            wireMockServer.stopRecording()
        }
    }

}
