package com.github.mcs.mavens

import com.fasterxml.jackson.databind.ObjectMapper
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.util.logging.Logger

/**
 * Central web client for Poker Maven's API access.
 *
 * @see <a href="https://www.briggsoft.com/docs/pmavens/Technical_Interface.htm#APICommands">
 *     Poker Mavens API command reference
 *     </a>
 */
class MavensApiClient(
    private val apiUrl: String,
    private val apiPassword: String
) {

    fun <T> execute(command: String, parameters: Map<String, String>, responseType: Class<T>): T? {
        val inputParameters = parameters
            .map { Pair(it.key, it.value) }
        val completeParameterList = listOf(
            Pair("Password", apiPassword),
            Pair("Command", command),
            Pair("JSON", "Yes")
        ).plus(inputParameters)

        val sb = StringBuilder()
        for ((key, value) in completeParameterList) {
            sb.append("&$key=$value")
        }

        val requestData = sb.substring(1)

        val bodyPublisher = HttpRequest.BodyPublishers
            .ofString(requestData)
        val httpRequest = HttpRequest.newBuilder(URI.create(apiUrl))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .POST(bodyPublisher)
            .build()
        val responseBodyHandler = HttpResponse.BodyHandlers.ofString()
        val httpResponse = HttpClient.newHttpClient().send(httpRequest, responseBodyHandler)

        if (httpResponse.statusCode() == 200) {
            return ObjectMapper().readValue(httpResponse.body(), responseType)
        } else {
            Logger.getLogger(this.javaClass.name).warning(
                """HTTP call finished with status code ${httpResponse.statusCode()}
                |RequestData was:
                |$requestData""".trimMargin()
            )
            return null
        }
    }
}