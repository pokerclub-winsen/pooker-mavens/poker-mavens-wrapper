package com.github.mcs.mavens.domain.account

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.github.mcs.mavens.domain.common.Result

@JsonIgnoreProperties(ignoreUnknown = true)
data class AccountsPasswordResponseDto(
    @JsonProperty("Result") val result: Result,
    @JsonProperty("Verified") val verified: String?
)
