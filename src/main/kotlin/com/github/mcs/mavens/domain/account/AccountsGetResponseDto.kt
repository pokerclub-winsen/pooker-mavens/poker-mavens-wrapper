package com.github.mcs.mavens.domain.account

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.github.mcs.mavens.domain.common.Result

@JsonIgnoreProperties(ignoreUnknown = true)
data class AccountsGetResponseDto(
    @JsonProperty("Result") val result: Result,
    @JsonProperty("Player") val player: String?,
    @JsonProperty("AdminProfile") val adminProfile: String?,
    @JsonProperty("Title") val title: String?,
    @JsonProperty("Level") val level: String?,
    @JsonProperty("RealName") val realName: String?,
    @JsonProperty("PWHash") val pWHash: String?,
    @JsonProperty("Gender") val gender: String?,
    @JsonProperty("Location") val location: String?,
    @JsonProperty("Balance") val balance: Int,
    @JsonProperty("Balance2") val balance2: Int,
    @JsonProperty("Permissions") val permissions: String?,
)
