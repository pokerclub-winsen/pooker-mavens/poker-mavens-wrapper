package com.github.mcs.mavens.domain.common

enum class Result {
    Ok, Error
}