package com.github.mcs.mavens.domain.account

import com.github.mcs.mavens.MavensApiClient
import com.github.mcs.mavens.domain.common.Result

class AccountsPasswordComponent(private val mavensApiClient: MavensApiClient) {

    fun check(username: String, password: String): Boolean {
        val (result, verified) = mavensApiClient.execute(
            "AccountsPassword",
            mapOf(
                "Player" to username,
                "PW" to password
            ),
            AccountsPasswordResponseDto::class.java
        ) ?: return false

        return result == Result.Ok && verified == "Yes"
    }
}