package com.github.mcs.mavens.domain.account

import com.github.mcs.mavens.MavensApiClient
import com.github.mcs.mavens.domain.common.Result

class AccountsGetComponent(private val mavensApiClient: MavensApiClient) {

    fun get(username: String): AccountsGetResponseDto? {
        val result = mavensApiClient.execute(
            "AccountsGet",
            mapOf(
                "Player" to username,
            ),
            AccountsGetResponseDto::class.java
        )

        return if (result != null && result.result == Result.Ok)
            result
        else
            null
    }
}